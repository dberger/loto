import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import { init } from "./loto.js";
import fs from "fs"
import https from "https"
const app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(cors());

app.get('/loto', async function (req, res) {
    await init(req.query.years).then(function (stats) {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(stats));
    }).catch(error => {console.log(error);res.send(JSON.stringify(error))});
})

if(process.env.HTTPS_ENABLED !== '0') {
// Certificate
    const privateKey = fs.readFileSync(process.env.HTTPS_PRIVATE_KEY, 'utf8');
    const certificate = fs.readFileSync(process.env.HTTPS_CERTIFICATE, 'utf8');
    const ca = fs.readFileSync(process.env.HTTPS_CA, 'utf8');

    const credentials = {
        key: privateKey,
        cert: certificate,
        ca: ca
    };
    https.createServer(credentials, app)
        .listen(1998, function () {
            console.log('Example app listening on port 1998! Go to https://localhost:1998/')
        })
} else {

    app.listen(1998);
}

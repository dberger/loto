import axios from 'axios';
import jsdom from 'jsdom'

const {JSDOM} = jsdom;

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0

const memoryStatsByYear = {}
const yearsUrl = new Map();

export const init = async (yearsSelected) => {
    if (yearsSelected && yearsSelected.length) {
        yearsSelected = Buffer.from(yearsSelected, 'base64').toString().split(',');
    } else {
        yearsSelected = [];
    }
    const promises = [];

    // Si l'année en cours n'est pas en mémoire, ou si on est sur une recherche et que la première année demandé n'est pas en mémoire
    if (!yearsUrl.get(new Date().getFullYear()) && (!yearsSelected.length || !yearsUrl.get(yearsSelected[0]))) {
        // On récupère toutes les années existantes sur le site
        await axios.get('https://www.tirage-euromillions.net/loto/annee/').then(function (response) {
            const dom = new JSDOM(response.data);
            const $elements = dom.window.document.querySelectorAll("a[href*='https://www.tirage-euromillions.net/loto/annee/tirages-']");
            $elements.forEach($elt => {
                yearsUrl.set($elt.textContent.split(' ').pop(), $elt.getAttribute('href'))
            })

        })
    }

    return new Promise((resolve, reject) => {
        // Si on est sur un recherche avec sélection d'années
        if (yearsSelected.length) {
            yearsSelected.forEach(year => {
                handleUrl(yearsUrl.get(year), promises, year)
            })
        } else {
            // On va aller récupérer les données de chaque année
            yearsUrl.forEach((url, key) => {
                handleUrl(url, promises, key)
            })
        }
        Promise.all(promises).then(() => {
            resolve(calculateStats(yearsSelected))
        }).catch(function (error) {
            reject(error)
        })
    })

}

const calculateStats = (yearsSelected) => {
    const mapPoints = new Map();
    const mapStar = new Map();
    const years = yearsSelected.length ? yearsSelected : Object.keys(memoryStatsByYear);
    years.forEach(year => {
        memoryStatsByYear[year].points.forEach((val, key) => {
            if (mapPoints.get(key)) {
                mapPoints.set(key, mapPoints.get(key) + val)
                return;
            }
            mapPoints.set(key, val)
        })
        memoryStatsByYear[year].stars.forEach((val, key) => {
            if (mapStar.get(key)) {
                mapStar.set(key, mapStar.get(key) + val)
                return;
            }
            mapStar.set(key, val)
        })
    })
    return {
        stars: Array
            .from(mapStar)
            .sort((a, b) => {
                return b[1] - a[1];
            }),
        stats: Array
            .from(mapPoints)
            .sort((a, b) => {
                return b[1] - a[1];
            }),
        years: years
    }
}

const handleUrl = (url, promises, year) => {
    promises.push(new Promise(async (resolve, reject) => {
            // Si on a les données en mémoire il n'est pas nécessaire de refetch et parser, en revanche si on est sur l'année en cours il faut aller actualiser
            if (!memoryStatsByYear[year] || (memoryStatsByYear[year] && parseInt(year) === new Date().getFullYear())) {

                await axios.get(url).then(function (response) {
                    const dom = new JSDOM(response.data);
                    const table = dom.window.document.querySelector(".blue_table");

                    const points = table.querySelectorAll('.game_point');
                    const stars = table.querySelectorAll('.star_small');
                    memoryStatsByYear[year] = {
                        points: new Map(),
                        stars: new Map()
                    };

                    points.forEach(pts => {
                        const point = parseInt(pts.textContent)
                        if (isNaN(point)) {
                            return;
                        }
                        if (memoryStatsByYear[year].points.get(point)) {
                            memoryStatsByYear[year].points.set(point, memoryStatsByYear[year].points.get(point) + 1)
                            return;
                        }
                        memoryStatsByYear[year].points.set(point, 1)
                    })
                    stars.forEach(item => {
                        const star = parseInt(item.textContent)
                        if (isNaN(star)) {
                            return;
                        }
                        if (memoryStatsByYear[year].stars.get(star)) {
                            memoryStatsByYear[year].stars.set(star, memoryStatsByYear[year].stars.get(star) + 1)
                            return;
                        }
                        memoryStatsByYear[year].stars.set(star, 1);
                    })
                })
            }

            resolve(true);
        }).catch(function (error) {
            console.log(error)
        })
    )
}
